# Politikorama

## Installation

### Development environment

Use a virtual environment

    python3 -m venv venv
    source venv/bin/activate

Install components

    pip install -r requirements-dev.txt

Create database (postgresql)

    CREATE USER politikorama WITH PASSWORD 'politikorama';
    CREATE DATABASE politikorama WITH OWNER politikorama ENCODING 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8' TEMPLATE='template0';

And start application

    flask run


### Production environment

Use a virtual environment

    python3 -m venv venv
    source venv/bin/activate

Install components

    pip install -r requirements.txt

Create database (postgresql)

    CREATE USER politikorama WITH PASSWORD 'supercomplexpassword';
    CREATE DATABASE politikorama WITH OWNER politikorama ENCODING 'UTF8' LC_COLLATE = 'fr_FR.UTF-8' LC_CTYPE = 'fr_FR.UTF-8' TEMPLATE='template0';

And start application and distribute it through some proxy

    gunicorn --workers 2 --bind 0.0.0.0:5000 server


## Testing

Using pytest

    pytest


## Code styling

Using black

    black --diff --exclude "venv/|migrations/" .


## Translation

It uses Flask-Babel for translations

## Extraction of strings to translate

    pybabel extract --ignore venv -F app/babel.cfg -o messages.pot ./

## Initialisation of a specific language

    pybabel init -i messages.pot -d translations -l fr

## Update of strings for all translated languages

    pybabel update -i messages.pot -d translations

## Compile each languages to use them

    pybabel compile -d translations
