# encoding: utf-8

from flask import g


# before_request action
def before():
    pass


# after_request action
def after(response):
    return response
