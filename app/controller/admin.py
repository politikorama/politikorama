# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller


class Admin(Controller):
    def dashboard(self):
        return redirect(url_for("core.index"))
