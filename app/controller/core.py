# encoding: utf-8

from datetime import date

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller
from app.form.stance import StanceNewForm
from app.model.representative import RepresentativeModel


class Core(Controller):
    def index(self):
        g.form = StanceNewForm()
        g.form.date.data = date.today()
        g.representatives = RepresentativeModel.query.limit(3).all()
        return render_template("core/index.html")
