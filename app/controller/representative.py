# encoding: utf-8

from datetime import date

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller
from app.model.representative import RepresentativeModel


class Representative(Controller):
    def view(self, id=None):
        g.representative = RepresentativeModel.query.get(id)
        return render_template("representative/view.html")
