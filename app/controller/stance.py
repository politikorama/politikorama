# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required

from app.controller.controller import Controller
from app.form.stance import StanceNewForm
from app.model.representative import RepresentativeModel
from app.model.stance import SourceModel, StanceModel


class Stance(Controller):
    def new(self):
        form = StanceNewForm()
        if form.validate_on_submit():
            representative = RepresentativeModel.query.filter_by(
                name=form.representative.data
            ).first()
            if representative is None:
                representative = RepresentativeModel(name=form.representative.data)
                representative.save()
            source = SourceModel.query.filter_by(name=form.source.data).first()
            if source is None:
                source = SourceModel(name=form.source.data)
                source.save()
            stance = StanceModel(
                author_id=representative.id,
                source_id=source.id,
                message=form.message.data,
                date=form.date.data,
            )
            stance.save()
        else:
            print(form.errors)
        return redirect(url_for("core.index"))
