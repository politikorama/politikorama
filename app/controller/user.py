# encoding: utf-8

from flask import flash, g, redirect, render_template, request, session, url_for
from flask_babel import gettext as _
from flask_login import current_user, login_required, login_user, logout_user

from app.controller.controller import Controller
from app.form.user import *
from app.model.user import UserModel


class User(Controller):
    def login(self):
        g.form = UserLoginForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is not None and user.check_password(g.form.password.data):
                    if user.is_active:
                        login_user(user)
                        return redirect(url_for("user.profile"))
                    else:
                        flash(
                            _(
                                "Votre compte a été désactivé. En cas de problème, "
                                "vous pouvez contacter l'équipe de support."
                            ),
                            "warning",
                        )
                else:
                    flash(
                        _("Identifiant inconnu ou mauvais mot de passe."),
                        "warning",
                    )
        return render_template("user/login.html")

    @login_required
    def logout(self):
        session.clear()
        return redirect(url_for("core.index"))

    def register(self):
        g.form = UserRegisterForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is None:
                    if g.form.password.data == g.form.password_confirmation.data:
                        user = UserModel(
                            login=g.form.login.data,
                            password=g.form.password.data,
                            email=g.form.email.data,
                            active=True,
                        )
                        user.save()
                        login_user(user)
                        return redirect(url_for("user.profile"))
                    else:
                        flash(
                            _("Les deux mots de passe ne sont pas identiques."),
                            "warning",
                        )
                else:
                    flash(
                        _(
                            "Cet identifiant n'est pas utilisable, "
                            "merci d'en sélectionner un autre."
                        ),
                        "warning",
                    )
        return render_template("user/register.html")

    def forgot_password(self):
        g.form = UserForgotPasswordForm()
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(login=g.form.login.data).first()
                if user is None:
                    user = UserModel.query.filter_by(email=g.form.login.data).first()
                if user is not None:
                    # TODO: send email & create token
                    flash(_("Un e-mail a été envoyé."), "success")
                return redirect(url_for("core.index"))

        return render_template("user/forgot_password.html")

    @login_required
    def profile(self):
        g.form = UserProfileForm(obj=current_user)
        if request.method == "POST":
            if g.form.validate_on_submit():
                user = UserModel.query.filter_by(id=current_user.id).first()
                user.email = g.form.email.data
                user.save()
                return redirect(url_for("user.profile"))
        return render_template("user/profile.html")
