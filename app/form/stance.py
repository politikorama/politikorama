# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import DateField, StringField
from wtforms.validators import DataRequired


class StanceNewForm(FlaskForm):
    representative = StringField(_("Représentant"), validators=[DataRequired()])
    date = StringField(_("Date"))
    source = StringField(_("Source"), validators=[DataRequired()])
    message = StringField(_("Prise de position"), validators=[DataRequired()])
