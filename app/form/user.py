# encoding: utf-8

from flask_babel import gettext as _
from flask_wtf import FlaskForm
from wtforms import BooleanField, HiddenField, PasswordField, StringField
from wtforms.validators import DataRequired


class UserLoginForm(FlaskForm):
    login = StringField(_("Identifiant"), validators=[DataRequired()])
    password = PasswordField(_("Mot de passe"), validators=[DataRequired()])


class UserRegisterForm(FlaskForm):
    login = StringField(_("Identifiant"), validators=[DataRequired()])
    password = PasswordField(_("Mot de passe"), validators=[DataRequired()])
    password_confirmation = PasswordField(
        _("Confirmation"),
        validators=[DataRequired()],
        description=_("Reprise du mot de passe pour confirmation."),
    )
    email = PasswordField(
        _("Adresse mail"),
        description=_("Optionnel."),
    )


class UserForgotPasswordForm(FlaskForm):
    login = StringField(_("Identifiant ou adresse mail"), validators=[DataRequired()])


class UserResetPasswordForm(FlaskForm):
    token = HiddenField()
    password = PasswordField(_("Mot de passe"), validators=[DataRequired()])
    password_confirmation = PasswordField(
        _("Confirmation"),
        validators=[DataRequired()],
        description=_("Reprise du mot de passe pour confirmation."),
    )


class UserProfileForm(FlaskForm):
    email = StringField(
        _("Adresse mail"),
        description=_("Optionnel."),
    )
