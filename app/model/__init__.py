# encoding: utf-8
"""
This module imports modles in order to allow flask to find them.
"""

import app.model.representative
import app.model.stance
import app.model.user
