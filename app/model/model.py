# encoding: utf-8

from datetime import datetime

from app import db


class Model:
    def save(self, *args, **kwargs):
        model = self
        is_existing = True
        if len(self.__table__.primary_key.columns.keys()) > 1:
            is_existing = False
        else:
            for column_name in self.__table__.primary_key.columns.keys():
                column = self.__getattribute__(column_name)
                if column is None or column == "":
                    is_existing = False
        if not is_existing:
            self.on_before_create(model)
            db.session.add(self)
        self.on_before_update(model)
        db.session.commit()
        if not is_existing:
            self.on_after_create(model)
        self.on_after_update(model)

    def delete(self, *args, **kwargs):
        model = self
        is_existing = True
        for column_name in self.__table__.primary_key.columns.keys():
            column = self.__getattribute__(column_name)
            if column is None or column == "":
                is_existing = False
        if is_existing:
            self.on_before_delete(model)
            db.session.delete(self)
            db.session.commit()
            self.on_after_delete(model)

    def on_before_create(self, model):
        pass

    def on_before_update(self, model):
        pass

    def on_before_delete(self, model):
        pass

    def on_after_create(self, model):
        pass

    def on_after_update(self, model):
        pass

    def on_after_delete(self, model):
        pass

    @classmethod
    def execute(self, query):
        db.session.execute(query)
        db.session.commit()

    @classmethod
    def truncate(self):
        db.session.execute(f"truncate table {self.__tablename__};")
        db.session.commit()

    def serialize(self):
        result_dict = {}
        for key in self.__table__.columns.keys():
            if not key.startswith("_"):
                if isinstance(getattr(self, key), datetime):
                    result_dict[key] = getattr(self, key).strftime("%Y-%m-%d")
                else:
                    result_dict[key] = getattr(self, key)
        return result_dict
