# encoding: utf-8

from slugify import slugify

from app.model.model import db, Model


class CountryModel(db.Model, Model):
    __tablename__ = "country"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))


class RepresentativeModel(db.Model, Model):
    __tablename__ = "representative"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    job = db.Column(db.String(200))
    country_id = db.Column(db.Integer, db.ForeignKey("country.id"))
    country = db.relationship("CountryModel", backref="representatives")

    @property
    def slug(self):
        return slugify(self.name)

    @property
    def picture(self):
        return f"pictures/{self.slug}.jpg"

    @property
    def active_stances(self):
        return list(filter(lambda r: r.moderated, self.stances))

    @property
    def entities(self):
        """
        Return the list of mandates grouped by entity and sorted by start, and end dates
        """
        entities = []
        for membership in self.memberships:
            for entity in entities:
                if membership.entity.name == entity["name"]:
                    entity["mandates"].append(
                        {
                            "role": membership.role.name,
                            "start": membership.start,
                            "end": membership.end,
                        }
                    )
                    break
            else:
                entities.append(
                    {
                        "name": membership.entity.name,
                        "mandates": [
                            {
                                "role": membership.role.name,
                                "start": membership.start,
                                "end": membership.end,
                            },
                        ]
                    }
                )
        # Sort mandates by start date
        for entity in entities:
            entity["mandates"].sort(key=lambda x: x["start"])
        return entities


class RoleModel(db.Model, Model):
    __tablename__ = "role"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))


class EntityModel(db.Model, Model):
    __tablename__ = "entity"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))


class MembershipModel(db.Model, Model):
    __tablename__ = "membership"
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship("RepresentativeModel", backref="memberships")
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"))
    role = db.relationship("RoleModel")
    entity_id = db.Column(db.Integer, db.ForeignKey("entity.id"))
    entity = db.relationship("EntityModel", backref="membership")
    start = db.Column(db.Date)
    end = db.Column(db.Date)
