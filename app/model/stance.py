# encoding: utf-8

from app.model.model import db, Model


class SourceModel(db.Model, Model):
    __tablename__ = "source"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))


class AccountModel(db.Model, Model):
    __tablename__ = "account"
    id = db.Column(db.Integer, primary_key=True)
    representative_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    representative = db.relationship("RepresentativeModel", backref="accounts")
    source_id = db.Column(db.Integer, db.ForeignKey("source.id"))
    source = db.relationship("SourceModel", backref="accounts")
    identifier = db.Column(db.String(200))


class StanceModel(db.Model, Model):
    __tablename__ = "stance"
    id = db.Column(db.Integer, primary_key=True)
    author_id = db.Column(db.Integer, db.ForeignKey("representative.id"))
    author = db.relationship("RepresentativeModel", backref="stances")
    source_id = db.Column(db.Integer, db.ForeignKey("source.id"))
    source = db.relationship("SourceModel", backref="stances")
    message_id = db.Column(db.String(200))
    message = db.Column(db.Text)
    date = db.Column(db.Date)
    moderated = db.Column(db.Boolean, default=False)
    moderation_note = db.Column(db.Text)
