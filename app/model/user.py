# encoding: utf-8

import bcrypt

from flask import current_app

from app.model.model import db, Model


def get_user(user_id):
    return UserModel.query.get(user_id)


class UserModel(db.Model, Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(100), unique=True)
    password_hash = db.Column(db.String(128), default="")
    email = db.Column(db.String(500), default="")
    active = db.Column(db.Boolean, default=False)
    admin = db.Column(db.Boolean, default=False)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.hashpw(
            password.encode("utf-8"),
            bcrypt.gensalt(rounds=int(current_app.config["BCRYPT_ROUNDS"])),
        ).decode("UTF-8")

    def check_password(self, password):
        return bcrypt.checkpw(
            password.encode("utf-8"),
            self.password_hash.encode("utf-8"),
        )

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_authenticated(self):
        return self.id is not None

    @property
    def is_admin(self):
        return self.admin

    def get_id(self):
        return str(self.id)

    def __repr__(self):
        return self.login
