# encoding: utf-8

from app.controller.admin import Admin
from app.controller.core import Core
from app.controller.representative import Representative
from app.controller.stance import Stance
from app.controller.user import User


# Listing endpoints
routes = [
    # Core
    ("/", Core.as_view("index"), ["GET", "POST"]),
    # Stance
    ("/stance/new", Stance.as_view("new"), ["POST"]),
    # Representative
    ("/representative/<int:id>", Representative.as_view("view")),
    # User
    ("/register", User.as_view("register"), ["GET", "POST"]),
    ("/login", User.as_view("login"), ["GET", "POST"]),
    ("/forgot_password", User.as_view("forgot_password"), ["GET", "POST"]),
    ("/logout", User.as_view("logout")),
    ("/profile", User.as_view("profile"), ["GET", "POST"]),
    # Admin
    ("/dashboard", Admin.as_view("dashboard"), ["GET", "POST"]),
]

# Listing API endpoints
apis = []
