# encoding: utf-8

import sys

import click
from flask.cli import with_appcontext

from app.model.user import UserModel


class Username(click.types.ParamType):
    def convert(self, value, param, ctx):
        # minimal verification
        if UserModel.query.filter_by(login=value).count() > 0:
            self.fail("That username is already taken")
        return value


class Email(click.types.ParamType):
    def convert(self, value, param, ctx):
        # minimal verification
        if "@" not in value:
            self.fail("Enter a valid email address")
        return value


@click.command(help="Create a superuser.")
@with_appcontext
def createsuperuser():
    def check_password(value, username, email):
        fine = True
        # minimal verifications
        if value in username:
            click.echo("This password is too similar to username.")
            fine = False
        if value in email:
            click.echo("This password is too similar to email address.")
            fine = False
        if len(value) < 8:
            click.echo(
                "This password is too short. It must contain at least 8 characters."
            )
            fine = False
        return fine

    username = click.prompt(
        "Username (leave blank to use 'admin')",
        type=Username(),
        default="admin",
        show_default=False,
    )
    email = click.prompt("Email address", type=Email())
    password = click.prompt(
        "Password",
        type=str,
        hide_input=True,
        confirmation_prompt="Password (again)",
    )
    while not check_password(password, username, email):
        bypass = click.prompt(
            "Bypass password validation and create user anyway? [y/N]",
            type=str,
            default="n",
            show_default=False,
        )
        if bypass.lower() == "y":
            break
        password = click.prompt(
            "Password",
            type=str,
            hide_input=True,
            confirmation_prompt="Password (again)",
        )
    user = UserModel(
        login=username,
        password=password,
        email=email,
        active=True,
        admin=True,
    )
    user.save()
    click.echo("Superuser created successfully.")
