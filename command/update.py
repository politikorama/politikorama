# encoding: utf-8

import click

from command.updater import french_assembly


@click.group()
def update():
    "Updating datas."
    pass


update.add_command(french_assembly.representatives)
