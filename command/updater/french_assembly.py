# encoding: utf-8

import json
from time import mktime
from datetime import datetime, timezone

import click
from flask.cli import with_appcontext

from app.model.representative import CountryModel, EntityModel, MembershipModel, RepresentativeModel, RoleModel


@click.command(help="Update representatives from file (json format).")
@click.argument("filename")
@with_appcontext
def representatives(filename):
    click.echo("French assembly update")
    with open(filename) as handler:
        datas = json.load(handler)
    with click.progressbar(datas) as bar:
        for data in bar:
            # Representative
            modified = False
            name = data["name"]
            representative = RepresentativeModel.query.filter_by(name=name).first()
            if representative is None:
                representative = RepresentativeModel(
                    name=name,
                )
                representative.save()
            if representative.job != data["job"]:
                modified = True
                representative.job = data["job"]
            country = CountryModel.query.filter_by(name=data["country"]).first()
            if country is None:
                country = CountryModel(name=data["country"])
                country.save()
            if representative.country_id != country.id:
                modified = True
                representative.country_id = country.id
            if modified:
                representative.save()

            # Mandates
            mandates = data["mandates"]
            for mandate in mandates:
                entity = EntityModel.query.filter_by(name=mandate["entity"]).first()
                if entity is None:
                    entity = EntityModel(name=mandate["entity"])
                    entity.save()
                role = RoleModel.query.filter_by(name=mandate["role"]).first()
                if role is None:
                    role = RoleModel(name=mandate["role"])
                    role.save()
                start = datetime.strptime(mandate["start"], "%Y-%m-%d")
                end = datetime.strptime(mandate["end"], "%Y-%m-%d") if mandate["end"] is not None else None
                membership = MembershipModel.query.filter_by(
                    representative_id=representative.id
                ).filter_by(
                    entity_id=entity.id
                ).filter_by(
                    role_id=role.id
                ).filter_by(
                    start=start
                ).first()
                if membership is None:
                    membership = MembershipModel(
                        representative_id=representative.id,
                        entity_id=entity.id,
                        role_id=role.id,
                        start=start,
                        end=end,
                    )
                    membership.save()
