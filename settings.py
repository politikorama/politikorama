# encoding: utf-8

import os

from dotenv import load_dotenv


load_dotenv()

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
SECRET_KEY = os.environ.get("SECRET_KEY", "Choose a secret key")
JINJA_ENV = {
    "TRIM_BLOCKS": True,
    "LSTRIP_BLOCKS": True,
}
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI",
    "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3"),
)
SQLALCHEMY_TRACK_MODIFICATIONS = False
BCRYPT_ROUNDS = os.environ.get("BCRYPT_ROUNDS", 6)
BABEL_DEFAULT_LOCALE = os.environ.get("BABEL_DEFAULT_LOCALE", "fr")
AVAILABLE_LANGUAGES = {
    "fr": "Français",
}
CDN = os.environ.get("CDN","static")
