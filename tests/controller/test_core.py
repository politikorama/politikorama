# encoding: utf-8


def test_request_homepage(client):
    response = client.get("/")
    assert response.status.startswith("200")
    assert b"Politikorama" in response.data
