# encoding: utf-8


def test_user_register(client):
    response = client.post(
        "/register",
        follow_redirects=True,
        data={
            "login": "test",
            "password": "my_password",
            "password_confirmation": "my_password",
        },
    )
    assert response.status.startswith("200")


def test_user_register_with_two_different_passwords(client):
    response = client.post(
        "/register",
        follow_redirects=True,
        data={
            "login": "test",
            "password": "my_password",
            "password_confirmation": "other_password",
        },
    )
    assert response.status.startswith("200")
    assert "pas identiques" in response.data.decode("utf-8")


def test_user_register_same_login(client):
    client.post(
        "/register",
        data={
            "login": "test",
            "password": "my_password",
            "password_confirmation": "my_password",
        },
    )
    response = client.post(
        "/register",
        follow_redirects=True,
        data={
            "login": "test",
            "password": "other_password",
            "password_confirmation": "other_password",
        },
    )
    assert response.status.startswith("200")
    assert "est pas utilisable" in response.data.decode("utf-8")
